interface SeedChar {
    name: string,
    description: string;
    age: number;
    gender: 'men'|'women'|'other'
    portrait: string;   
}


interface SeedData {
    chars: SeedChar[];
}


export const initialData: SeedData = {
    chars: [
        {
            name: "ryu",
            description: "The answer lies in the heart of battle",
            age: 38,
            gender: "men",
            portrait: "imgUrl.jpg",
        },
        {
            name: "ken",
            description: "Sure you can",
            age: 38,
            gender: "men",
            portrait: "imgUrl.jpg",
        },
        {
            name: "guile",
            description: "A family man",
            age: 40,
            gender: "men",
            portrait: "imgUrl.jpg",
        },
        {
            name: "chun li",
            description: "I'll avenge my father",
            age: 34,
            gender: "women",
            portrait: "imgUrl.jpg",
        },
        {
            name: "blanka",
            description: "Random description",
            age: 10,
            gender: "other",
            portrait: "imgUrl.jpg",
        },
        {
            name: "honda",
            description: "The answer lies in the heart of battle",
            age: 38,
            gender: "men",
            portrait: "imgUrl.jpg",
        },
        {
            name: "bison",
            description: "The answer lies in the heart of battle",
            age: 38,
            gender: "men",
            portrait: "imgUrl.jpg",
        },
        {
            name: "sagat",
            description: "The answer lies in the heart of battle",
            age: 38,
            gender: "men",
            portrait: "imgUrl.jpg",
        },
    ]
}