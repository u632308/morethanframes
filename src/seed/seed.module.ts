import { Module } from '@nestjs/common';
import { SeedService } from './seed.service';
import { SeedController } from './seed.controller';
import { CharsModule } from 'src/chars/chars.module';

@Module({
  controllers: [SeedController],
  providers: [SeedService],
  imports: [
    CharsModule
  ]
})
export class SeedModule {}
