import { Injectable } from '@nestjs/common';
import { CharsService } from '../chars/chars.service';
import { initialData } from './data/seed-data';

@Injectable()
export class SeedService {

  constructor(
    private readonly charsService: CharsService
  ) {}
  
  async runSeed() {
    await this.insertNewChars();
    return 'SEED EXECUTED'
  }

  private async insertNewChars() {
    const chars = initialData.chars;

    const insertPromises = [];

    chars.forEach( char => {
      insertPromises.push(this.charsService.create(char))
    })

    await Promise.all( insertPromises )

    return true;
  }
}
