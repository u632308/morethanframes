import { BadRequestException, Injectable, InternalServerErrorException, Logger, NotFoundException } from '@nestjs/common';
import { CreateCharDto } from './dto/create-char.dto';
import { UpdateCharDto } from './dto/update-char.dto';
import { CreateMoveDto } from './dto/create-move.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Char, CharMove } from './entities';
import { validate as isUUID } from 'uuid';


@Injectable()
export class CharsService {

  private readonly logger = new Logger('CharsService');

  constructor(

    @InjectRepository(Char)
    private readonly charRepository: Repository<Char>,
    @InjectRepository(CharMove)
    private readonly moveRepository: Repository<CharMove>,
  ) {}

  async create(createCharDto: CreateCharDto) {
    try {

      const char = this.charRepository.create(createCharDto);
      await this.charRepository.save( char );

      return char;
      
    } catch (error) {
      this.handleDBExceptions(error);
    }
  }

  findAll() {
    return this.charRepository.find({});
  }

  async findOne(term: string) {
    let char: Char;

    if (isUUID(term)) {
      char = await this.charRepository.findOneBy({ id: term })
    } else {
      char = await this.charRepository.findOneBy({ name: term })
    }

    if (!char)
      throw new NotFoundException(`Char with ${term} not found`)
    return char
  }

  async update(id: string, updateCharDto: UpdateCharDto) {
    const char = await this.charRepository.preload({
      id: id,
      ...updateCharDto
    });

    if ( !char ) throw new NotFoundException(`Char with id: ${ id } not found`);

    try {
      await this.charRepository.save( char );
      return char;
      
    } catch (error) {
      this.handleDBExceptions(error);
    } 
  }

  async remove(id: string) {
    const char = await this.findOne( id );
    await this.charRepository.remove( char );
    
  }

  // moves
  async createMove(createMoveDto: CreateMoveDto) {
    try {

      const move = this.moveRepository.create(createMoveDto);
      await this.moveRepository.save( move );

      return move;
      
    } catch (error) {
      this.handleDBExceptions(error);
    }
  }

  private handleDBExceptions( error: any ) {
    if ( error.code === '23505' )
      throw new BadRequestException(error.detail);

    this.logger.error(error)
    throw new InternalServerErrorException('Unexpected error, check server logs');
  }
}
