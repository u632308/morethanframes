import { Controller, Get, Post, Body, Patch, Param, Delete, ParseUUIDPipe } from '@nestjs/common';
import { CharsService } from './chars.service';
import { CreateCharDto } from './dto/create-char.dto';
import { UpdateCharDto } from './dto/update-char.dto';
import { CreateMoveDto } from './dto/create-move.dto';

@Controller('chars')
export class CharsController {
  constructor(private readonly charsService: CharsService) {}

  @Post()
  create(@Body() createCharDto: CreateCharDto) {
    return this.charsService.create(createCharDto);
  }

  @Get()
  findAll() {
    return this.charsService.findAll();
  }

  @Get(':term')
  findOne(@Param('term' ) term: string) {
    return this.charsService.findOne(term);
  }

  @Patch(':id')
  update(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updateCharDto: UpdateCharDto) {
    return this.charsService.update( id, updateCharDto);
  }

  @Delete(':id')
  remove(@Param('id', ParseUUIDPipe) id: string) {
    return this.charsService.remove(id);
  }

  // Moves
  @Post('move')
  createMove(@Body() createMoveDto: CreateMoveDto) {
    return this.charsService.createMove(createMoveDto);
  }

}
