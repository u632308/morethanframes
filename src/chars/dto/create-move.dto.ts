import { IsInt, IsOptional, 
    IsPositive, IsString, MinLength 
} from 'class-validator';

export class CreateMoveDto {
    @IsString()
    @MinLength(1)
    name: string;

    @IsString()
    @MinLength(1)
    comand: string;

    @IsString()
    @MinLength(1)
    charId: string;

    @IsString()
    @IsOptional()
    image?: string; 

    @IsInt()
    @IsPositive()
    @IsOptional()
    startup?: number; 

    @IsInt()
    @IsOptional()
    advantageOB?: number; 

    @IsInt()
    @IsOptional()
    advantageOH?: number; 

}
