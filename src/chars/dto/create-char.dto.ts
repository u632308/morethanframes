import { IsIn, IsInt, IsOptional, 
    IsPositive, IsString, MinLength 
} from 'class-validator';

export class CreateCharDto {
    @IsString()
    @MinLength(1)
    name: string;

    @IsString()
    @IsOptional()
    description?: string;

    @IsInt()
    @IsPositive()
    @IsOptional()
    age?: number; 

    @IsIn(['men','women','other'])
    gender: string;

    @IsString()
    @IsOptional()
    portrait?: string; 
}
