import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { CharsService } from './chars.service';
import { CharsController } from './chars.controller';

import { Char, CharMove } from './entities';

@Module({
  controllers: [CharsController],
  providers: [CharsService],
  imports: [
    TypeOrmModule.forFeature([ Char, CharMove ])
  ],
  exports: [
    CharsService,
    TypeOrmModule
  ]
})
export class CharsModule {}
