import { BeforeInsert, BeforeUpdate, Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { CharMove } from './char-move.entity';

@Entity()
export class Char {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('text', {
        unique: true,
    })
    name: string;

    @Column({
        type: 'text',
        nullable: true
    })
    description: string;

    @Column('int', {
        nullable: true
    })
    age: number;

    @Column('text')
    gender: string;

    @Column('text')
    portrait: string;

    // images
    @OneToMany(
        () => CharMove,
        (charMove) => charMove.char,
        { cascade: true }
    )
    moves?: CharMove[]

    @BeforeInsert()
    checkNameInsert() {
        this.name = this.name
            .toLowerCase()
    }

    @BeforeUpdate()
    checkNameUpdate() {
        this.name = this.name
            .toLowerCase()
    }

    

}
