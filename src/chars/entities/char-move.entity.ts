import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Char } from './char.entity';

@Entity()
export class CharMove {
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column('text', {
        nullable: true
    })
    image: string;

    @Column('text')
    name: string;

    @Column('text')
    comand: string;

    @Column('int', {
        nullable: true
    })
    startup: number;

    @Column('int', {
        nullable: true
    })
    advantageOB: number;

    @Column('int', {
        nullable: true
    })
    advantageOH: number;
    
    @Column('text')
    charId: string;

    @ManyToOne(
        () => Char,
        ( char ) => char.moves,
        { onDelete: 'CASCADE' }
    )
    char: Char

}
